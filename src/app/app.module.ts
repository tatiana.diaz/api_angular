import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { usuariosComponent } from './usuarios/usuarios.component';
import { CrearUsuariosComponent } from './crear-usuario/crear-usuario.component';
import {HttpClientModule} from '@angular/common/http';
import { Route, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';
import { AsociacionComponent } from './asociacion/asociacion.component';

const routes: Route[] = [
  {path: '',component:HomeComponent},
  {path: 'home',component:HomeComponent},
  {path: 'usuarios',component:usuariosComponent},
  {path: 'crear_usuario',component:CrearUsuariosComponent},
  {path: 'editar_usuario/:id',component:EditarUsuarioComponent},
  {path: 'asociacion',component:AsociacionComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    usuariosComponent,
    CrearUsuariosComponent,
    HomeComponent,
    EditarUsuarioComponent,
    AsociacionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
