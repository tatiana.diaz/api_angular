import { Component, OnInit } from '@angular/core';
import { Usuarios } from '../interfaces/Usuarios';
import { UsuariosService } from '../services/usuarios.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class usuariosComponent implements OnInit {
  usuarios:Usuarios[];
  
  constructor(private usuarios_service:UsuariosService,private _location: Location) {
  }

  ngOnInit() {
    this.getAllusuarios();
  }

  private getAllusuarios() {
    this.usuarios_service.getEstudiantes().subscribe(data=>{
      this.usuarios = data
    })
  }
  eliminarUsuario(id){
    this.usuarios_service.eliminar(id).subscribe(data=>{
    })
    setTimeout(()=>this.backClicked(),3000)
    } 

    backClicked() {
        window.location.reload();
    }
}
