import { ComponentFixture, TestBed } from '@angular/core/testing';

import { usuariosComponent } from './usuarios.component';

describe('usuariosComponent', () => {
  let component: usuariosComponent;
  let fixture: ComponentFixture<usuariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ usuariosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(usuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
