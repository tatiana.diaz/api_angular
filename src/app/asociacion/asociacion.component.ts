import { Component, OnInit } from '@angular/core';
import { Asociacion } from '../interfaces/Asociacion';
import { UsuariosService } from '../services/usuarios.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-asociacion',
  templateUrl: './asociacion.component.html',
  styleUrls: ['./asociacion.component.css']
})
export class AsociacionComponent implements OnInit {

  data:Asociacion = {
    id_estudiante : null,
    id_curso : null,
};

constructor(private usuario_service:UsuariosService,private _location: Location) { }

ngOnInit() {
  
}

crearUsuario(){
  this.usuario_service.asociacion(this.data).subscribe(response=>{
  });
   setTimeout(()=>this.backClicked(),3000)
  } 
  backClicked() {
      this._location.back();
  }
}
