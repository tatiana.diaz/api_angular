import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../services/usuarios.service';
import {Location} from '@angular/common';
import { Usuarios } from '../interfaces/Usuarios';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {
  Usuario;
  id = this.route.snapshot.paramMap.get("id");

  constructor(private usuario_service:UsuariosService,private _location: Location,private route: ActivatedRoute) { }

  ngOnInit() {
    this.getbyIdusuarios();
  }

  private getbyIdusuarios() {
    this.usuario_service.getUsuariosByID(this.id).subscribe(data=>{
      this.Usuario= data
    })
  }
  editarUsuario(){
    this.usuario_service.edit(this.Usuario).subscribe(response=>{
    });
     setTimeout(()=>this.backClicked(),5000)
    } 
    backClicked() {
        this._location.back();
    }

}
